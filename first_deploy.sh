#!/bin/bash
# Этот скрипт вызывает GitLab CI для первого деплоя приложения
cd ~/channelbot/channel-bot
docker compose stop
docker compose down
docker compose -f docker-compose-first-deploy.yml pull
docker compose -f docker-compose-first-deploy.yml build
docker compose -f docker-compose-first-deploy.yml up -d
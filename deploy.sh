#!/bin/bash
# Этот скрипт вызывает GitLab CI для деплоя обновлений
cd ~/channelbot/channel-bot
docker compose stop
docker compose down
docker container prune -f
docker image rm $(docker image ls -f dangling=true -q)
docker image rm $(docker image ls -q)
docker system prune -f
docker compose -f docker-compose.yml pull
docker compose -f docker-compose.yml build
docker compose -f docker-compose.yml up -d
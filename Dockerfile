FROM python:3.10.4-bullseye
LABEL maintainer="chernyakov@machine.com"
RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
RUN pip3 install --upgrade pip
RUN groupadd -r bot && useradd --no-log-init -r -g bot bot
RUN chown -R bot:bot /opt/venv
WORKDIR /home/bot/channelbot
COPY --chown=bot:bot . .
RUN chmod -R 744 /home/bot/channelbot
RUN /opt/venv/bin/pip3 install --no-cache-dir -r requirements.txt
import logging, logging.handlers, urllib.parse

LOG_FILENAME = 'bot.log'

# Установка определенного логера с желаемым уровнем вывода
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(LOG_FILENAME, backupCount=50, maxBytes=10000000)

logger.addHandler(handler)

# Токен бота
BOT_KEY = '.key_newsbot'

DEVELOPER_CHAT_ID = 123

# Количество постов в одном дайджесте
DIGEST_LIMIT = 10

# Enable/disable debugging
BOT_DEBUG = True
BOT_EXC_TIMEOUT = 20
BOT_TIMEOUT = 2

PYTHON_EXEC = 'python3'

# База данных
username = urllib.parse.quote_plus('root')
password = urllib.parse.quote_plus('P@ssW0rd')
MONGO_HOST = "mongodb://" + username + ":" + password + "@mongo:27017/?authSource=admin"
MONGO_PORT = 27017

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'

AUTOPOST = True
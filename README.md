# channel-bot
Форк FeedRetranslatorBot. Транслирует вашу RSS-ленту или новости на web-странице в Telegram-каналы.

Все сообщения внутри бота на русском языке и не переведены на другие языки.

Установка:
1. Получите токен бота с помощью Telegram BotFather https://t.me/BotFather
2. Скопируйте токен бота в файл .key_newsbot, который находится в той же директории, что и news2rsscmd.py
3. Установка и запуск автоматизированы. Для инициализации бота необходимо добавить ваш публичный useid В Telegram в поле command в блоке bot файла docker-compose-first-deploy.yml. Запустить команду 'python3 news2rsscmd.py init <username>' можно опционально, в контейнере с ботом, где username ваш публичный useid В Telegram (начинается со знака @, но без него). Например: python3 news2rsscmd.py init ibegtin 
4. Вы можете также запустить бот с помощью команды 'python3 news2rssbot.py'
5. Свяжите ваш бот через Telegram и используйте его команды для добавления каналов и лент. Используйте команду '/help' для начала.

Требования:
 - MongoDB
 - mongoengine
 - newsworker (https://github.com/ivbeg/newsworker)
 - click
 - python-telegram-bot
#!/bin/bash
# Этот скрипт установит Python 3.10.2 на Debian.
# Следует учесть, что скрипт установит конкретную версию Python, и написан с учётом, что на сервере с Debian одно ядро и всё будет выполняться от root-пользователя.
# Переходим в корневой каталог
cd ~
# Устанавливаем зависимости, необходимые для сборки Python:
apt update
apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev
# Загружаем исходный код последней версии со страницы загрузки Python с помощью wget, предварительно устанавливая wget.
apt install wget
wget https://www.python.org/ftp/python/3.10.2/Python-3.10.2.tgz
# После завершения загрузки распаковываем архив с gzip
tar -xf Python-3.10.2.tgz
# Удаляем уже распакованный архив
rm Python-3.10.2.tgz
# Переходим в исходный каталог Python и выполняем сценарий configure
cd Python-3.10.2
./configure --enable-optimizations
# Запускаем процесс сборки
make
# Когда процесс сборки будет завершен, устанавливаем двоичные файлы Python
make altinstall
# Проверяем версию Python
python3.10 --version
